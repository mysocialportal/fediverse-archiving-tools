# Fediverse Archiving Tools

This repository contains tools for working with locally archived data
from [Fediverse](https://en.wikipedia.org/wiki/Fediverse) accounts.

## Archive Browser

* [Fediverse Archive Browser](https://gitlab.com/mysocialportal/fediverse-archiving-tools/-/tree/main/fediverse_archive_browser): a desktop application that runs on Windows, Mac, and Linux to browse your archives, extract information, see statistics on it etc.


## Archive Generators/Updaters

For fediverse platforms that don't have the ability to export data or 
export it completely/easily. For those there are some archive generators:
* [Friendica Archiver](https://gitlab.com/mysocialportal/fediverse-archiving-tools/-/tree/main/friendica_archiver): a tool to create and update your YFriendica archive


# Licensing

All tools have a license file in their respective folders, as well as this top level one that reflect that these tools are licensed under [The Mozilla Public License Version 2.0 (MPLv2.0)](https://www.mozilla.org/en-US/MPL/2.0/)
