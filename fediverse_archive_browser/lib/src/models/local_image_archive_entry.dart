class ImageEntry {
  final String postId;
  final String localFilename;
  final String url;

  ImageEntry(
      {required this.postId, required this.localFilename, required this.url});

  ImageEntry.fromJson(Map<String, dynamic> json)
      : postId = json['postId'] ?? '',
        localFilename = json['localFilename'] ?? '',
        url = json['url'] ?? '';

  Map<String, dynamic> toJson() => {
        'postId': postId,
        'localFilename': localFilename,
        'url': url,
      };
}
