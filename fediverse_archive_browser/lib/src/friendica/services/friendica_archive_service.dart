import 'dart:convert';
import 'dart:io';

import 'package:fediverse_archive_browser/src/friendica/serializers/friendica_timeline_entry_serializer.dart';
import 'package:fediverse_archive_browser/src/friendica/services/friendica_path_mapping_service.dart';
import 'package:fediverse_archive_browser/src/models/entry_tree_item.dart';
import 'package:fediverse_archive_browser/src/models/local_image_archive_entry.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_interface.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';
import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:path/path.dart' as p;
import 'package:result_monad/result_monad.dart';

class FriendicaArchiveService implements ArchiveService {
  @override
  final FriendicaPathMappingService pathMappingService;

  final Map<String, ImageEntry> _imagesByRequestUrl = {};
  final List<EntryTreeItem> _postEntries = [];
  final List<EntryTreeItem> _orphanedCommentEntries = [];
  final List<EntryTreeItem> _allComments = [];
  @override
  final ConnectionsManager connectionsManager = ConnectionsManager();

  String _ownersName = '';

  FriendicaArchiveService({required this.pathMappingService});

  String get ownersName {
    if (_ownersName.isNotEmpty) {
      return _ownersName;
    }

    final uniqueNames = _postEntries.map((e) => e.entry.author).toSet();
    _ownersName = uniqueNames.isNotEmpty ? uniqueNames.first : '';

    return _ownersName;
  }

  void clearCaches() {
    connectionsManager.clearCaches();
    _imagesByRequestUrl.clear();
    _orphanedCommentEntries.clear();
    _allComments.clear();
    _postEntries.clear();
  }

  FutureResult<List<EntryTreeItem>, ExecError> getPosts() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_postEntries);
  }

  FutureResult<List<EntryTreeItem>, ExecError> getAllComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_allComments);
  }

  FutureResult<List<EntryTreeItem>, ExecError> getOrphanedComments() async {
    if (_postEntries.isEmpty && _allComments.isEmpty) {
      _loadEntries();
    }

    return Result.ok(_orphanedCommentEntries);
  }

  Result<ImageEntry, ExecError> getImageByUrl(String url) {
    if (_imagesByRequestUrl.isEmpty) {
      _loadImages();
    }

    final result = _imagesByRequestUrl[url];
    return result == null
        ? Result.error(ExecError(errorMessage: '$url not found'))
        : Result.ok(result);
  }

  String get _baseArchiveFolder => pathMappingService.rootFolder;

  void _loadEntries() {
    final entriesJsonPath = p.join(_baseArchiveFolder, 'postsAndComments.json');
    final jsonFile = File(entriesJsonPath);
    if (jsonFile.existsSync()) {
      final json = jsonDecode(jsonFile.readAsStringSync()) as List<dynamic>;
      final entries = json
          .map((j) => timelineEntryFromFriendicaJson(j, connectionsManager));
      final topLevelEntries =
          entries.where((element) => element.parentId.isEmpty);
      final commentEntries =
          entries.where((element) => element.parentId.isNotEmpty).toList();
      final entryTrees = <String, EntryTreeItem>{};

      final postTreeEntries = <EntryTreeItem>[];
      for (final entry in topLevelEntries) {
        final treeEntry = EntryTreeItem(entry, false);
        entryTrees[entry.id] = treeEntry;
        postTreeEntries.add(treeEntry);
      }

      final commentTreeEntries = <EntryTreeItem>[];
      commentEntries.sort(
          (c1, c2) => c1.creationTimestamp.compareTo(c2.creationTimestamp));
      for (final entry in commentEntries) {
        final parent = entryTrees[entry.parentId];
        final treeEntry = EntryTreeItem(entry, parent == null);
        parent?.addChild(treeEntry);
        entryTrees[entry.id] = treeEntry;
        commentTreeEntries.add(treeEntry);
      }

      _postEntries.clear();
      _postEntries.addAll(postTreeEntries);

      _allComments.clear();
      _allComments.addAll(commentTreeEntries);

      _orphanedCommentEntries.clear();
      _orphanedCommentEntries
          .addAll(entryTrees.values.where((element) => element.isOrphaned));
    }
  }

  void _loadImages() {
    final imageJsonPath = p.join(_baseArchiveFolder, 'images.json');
    final jsonFile = File(imageJsonPath);
    if (jsonFile.existsSync()) {
      final json = jsonDecode(jsonFile.readAsStringSync()) as List<dynamic>;
      final imageEntries = json.map((j) => ImageEntry.fromJson(j));
      for (final entry in imageEntries) {
        _imagesByRequestUrl[entry.url] = entry;
      }
    }
  }
}
