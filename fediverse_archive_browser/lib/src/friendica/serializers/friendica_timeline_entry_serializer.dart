import 'package:fediverse_archive_browser/src/friendica/serializers/friendica_contact_serializer.dart';
import 'package:fediverse_archive_browser/src/friendica/serializers/friendica_media_attachment_serializer.dart';
import 'package:logging/logging.dart';

import '../../models/location_data.dart';
import '../../models/timeline_entry.dart';
import '../../services/connections_manager.dart';
import '../../utils/offsetdatetime_utils.dart';

final _logger = Logger('FriendicaTimelineEntrySerializer');

TimelineEntry timelineEntryFromFriendicaJson(
    Map<String, dynamic> json, ConnectionsManager connections) {
  final int timestamp = json.containsKey('created_at')
      ? OffsetDateTimeUtils.epochSecTimeFromFriendicaString(json['created_at'])
          .fold(
              onSuccess: (value) => value,
              onError: (error) {
                _logger.severe("Couldn't read date time string: $error");
                return 0;
              })
      : 0;
  final id = json['id_str'] ?? '';
  final isReshare = json.containsKey('retweeted_status');
  final parentId = json['in_reply_to_status_id_str'] ?? '';
  final parentAuthor = json['in_reply_to_screen_name'] ?? '';
  final parentAuthorId = json['in_reply_to_user_id_str'] ?? '';
  final body = json['friendica_html'] ?? '';
  final author = json['user']['name'];
  final authorId = json['user']['id_str'];
  final title = json['friendica_title'] ?? '';
  final externalLink = json['external_url'] ?? '';
  final actualLocationData = LocationData();
  final modificationTimestamp = timestamp;
  final backdatedTimestamp = timestamp;
  final mediaAttachments = (json['attachments'] as List<dynamic>? ?? [])
      .map((j) => mediaAttachmentfromFriendicaJson(j))
      .toList();
  final likes = (json['friendica_activities']?['like'] as List<dynamic>? ?? [])
      .map((json) => contactFromFriendicaJson(json))
      .toList();
  final dislikes =
      (json['friendica_activities']?['dislike'] as List<dynamic>? ?? [])
          .map((json) => contactFromFriendicaJson(json))
          .toList();
  final announce =
      (json['friendica_activities']?['announce'] as List<dynamic>? ?? [])
          .map((json) => contactFromFriendicaJson(json))
          .toList();

  connections.addAllConnections([...likes, ...dislikes, ...announce]);

  return TimelineEntry(
    creationTimestamp: timestamp,
    modificationTimestamp: modificationTimestamp,
    backdatedTimestamp: backdatedTimestamp,
    locationData: actualLocationData,
    body: body,
    isReshare: isReshare,
    id: id,
    parentId: parentId,
    parentAuthorId: parentAuthorId,
    externalLink: externalLink,
    author: author,
    authorId: authorId,
    parentAuthor: parentAuthor,
    title: title,
    likes: likes,
    dislikes: dislikes,
    mediaAttachments: mediaAttachments,
  );
}
