import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/archive_types_enum.dart';
import 'package:logging/logging.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'video_player_settings.dart';

class SettingsService {
  static const archiveTypeKey = "archiveType";
  static const themeDarknessKey = 'themeDarkness';
  static const rootFolderKey = 'rootFolder';
  static const videoPlayerSettingTypeKey = 'videoPlayerSettingType';
  static const videoPlayerCommandKey = 'videoPlayerCustomPath';
  static const logLevelKey = "logLevel";

  Future<Level> logLevel() async {
    const defaultLevelIndex = 5; //INFO
    final prefs = await SharedPreferences.getInstance();
    final levelIndex = prefs.getInt(logLevelKey) ?? defaultLevelIndex;
    if (levelIndex > Level.LEVELS.length - 1 || levelIndex < 0) {
      return Level.INFO;
    }

    return Level.LEVELS[levelIndex];
  }

  Future<void> updateLevel(Level newLevel) async {
    final prefs = await SharedPreferences.getInstance();
    final index = Level.LEVELS.indexOf(newLevel);
    prefs.setInt(logLevelKey, index);
  }

  Future<ThemeMode> themeMode() async {
    final prefs = await SharedPreferences.getInstance();
    final themeIndex = prefs.getInt(themeDarknessKey) ?? 0;
    if (themeIndex > ThemeMode.values.length - 1 || themeIndex < 0) {
      return ThemeMode.system;
    }

    return ThemeMode.values[themeIndex];
  }

  Future<void> updateThemeMode(ThemeMode theme) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(themeDarknessKey, theme.index);
  }

  Future<ArchiveType> archiveType() async {
    final prefs = await SharedPreferences.getInstance();
    final archiveTypeIndex = prefs.getInt(archiveTypeKey) ?? 0;
    if (archiveTypeIndex > ArchiveType.values.length - 1 ||
        archiveTypeIndex < 0) {
      return ArchiveType.unknown;
    }

    return ArchiveType.values[archiveTypeIndex];
  }

  Future<void> updateArchiveType(ArchiveType archiveType) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(archiveTypeKey, archiveType.index);
  }

  Future<String> rootFolder() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getString(rootFolderKey) ?? '';
    return result;
  }

  Future<void> updateRootFolder(String folder) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(rootFolderKey, folder);
  }

  Future<VideoPlayerSettingType> videoPlayerSettingType() async {
    final prefs = await SharedPreferences.getInstance();
    if (!prefs.containsKey(videoPlayerSettingTypeKey)) {
      return _platformDefaultVideoType();
    }
    final type = prefs.getInt(videoPlayerSettingTypeKey) ?? 0;
    if (type > VideoPlayerSettingType.values.length - 1 || type < 0) {
      return _platformDefaultVideoType();
    }

    return VideoPlayerSettingType.values[type];
  }

  Future<void> updateVideoPlayerSettingType(
      VideoPlayerSettingType videoPlayerType) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setInt(videoPlayerSettingTypeKey, videoPlayerType.index);
  }

  Future<String> videoPlayerCommand() async {
    final prefs = await SharedPreferences.getInstance();
    final result = prefs.getString(videoPlayerCommandKey);
    if (result != null) {
      return result;
    }

    final currentType = await videoPlayerSettingType();

    return currentType.toAppPath();
  }

  Future<void> updateVideoPlayerCommand(String videoPlayerCommand) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(videoPlayerCommandKey, videoPlayerCommand);
  }

  VideoPlayerSettingType _platformDefaultVideoType() {
    if (Platform.isWindows) {
      return VideoPlayerSettingType.windows;
    }

    if (Platform.isMacOS) {
      return VideoPlayerSettingType.macOS;
    }

    if (Platform.isLinux) {
      return VideoPlayerSettingType.linuxVlc;
    }

    return VideoPlayerSettingType.custom;
  }
}
