import '../../models/media_attachment.dart';

MediaAttachment mediaAttachmentfromMastodonJson(Map<String, dynamic> json) {
  final uri = Uri.parse(json['url']);
  const creationTimestamp = 0;
  final explicitType = (json['mediaType'] ?? '').startsWith('image')
      ? AttachmentMediaType.image
      : (json['mimetype'] ?? '').startsWith('video')
          ? AttachmentMediaType.video
          : AttachmentMediaType.unknown;
  final thumbnailUri = Uri();
  final title = json['name'] ?? '';
  final description = json['blurhash'] ?? '';

  return MediaAttachment(
      uri: uri,
      creationTimestamp: creationTimestamp,
      metadata: {},
      thumbnailUri: thumbnailUri,
      title: title,
      explicitType: explicitType,
      description: description);
}
