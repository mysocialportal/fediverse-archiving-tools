import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/media_attachment.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_provider.dart';
import 'package:fediverse_archive_browser/src/services/path_mapper_service_interface.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';
import 'package:logging/logging.dart';
import 'package:provider/provider.dart';

class MediaWrapperComponent extends StatelessWidget {
  static final _logger = Logger('$MediaWrapperComponent');

  static const double _noPreferredValue = -1.0;
  final MediaAttachment mediaAttachment;
  final double preferredWidth;
  final double preferredHeight;

  const MediaWrapperComponent(
      {Key? key,
      required this.mediaAttachment,
      this.preferredWidth = _noPreferredValue,
      this.preferredHeight = _noPreferredValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final settingsController = Provider.of<SettingsController>(context);
    final archiveService = Provider.of<ArchiveServiceProvider>(context);
    final videoPlayerCommand = settingsController.videoPlayerCommand;
    final path = _calculatePath(archiveService);
    final width =
        preferredWidth > 0 ? preferredWidth : MediaQuery.of(context).size.width;
    final height = preferredHeight > 0
        ? preferredHeight
        : MediaQuery.of(context).size.height;

    if (mediaAttachment.explicitType == AttachmentMediaType.unknown) {
      return Text('Unable to resolve type for ${mediaAttachment.uri.path}');
    }

    if (mediaAttachment.explicitType == AttachmentMediaType.video) {
      final title = "Video (click to play): " + mediaAttachment.title;
      final thumbnailImageResult = _uriToImage(
          mediaAttachment.thumbnailUri.toString(), archiveService.pathMappingService,
          imageTypeName: 'thumbnail image');
      if (thumbnailImageResult.image != null) {
        return _createFinalWidget(
            baseContext: context,
            imageAndPath: thumbnailImageResult,
            width: width,
            height: height,
            noImageText: 'No Thumbnail',
            noImageOnTapText:
                'Click to launch video in external player (No Thumbnail)',
            onTap: () async => await _attemptToPlay(
                context, videoPlayerCommand, path.toString()));
      }

      return TextButton(
        onPressed: () async {
          await _attemptToPlay(context, videoPlayerCommand, path.toString());
        },
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          Text(mediaAttachment.description)
        ]),
      );
    }

    if (mediaAttachment.explicitType == AttachmentMediaType.image) {
      final imageResult = _uriToImage(path, archiveService.pathMappingService);
      if (imageResult.image == null) {
        final errorPath = imageResult.path.isNotEmpty
            ? imageResult.path
            : mediaAttachment.uri.toString();
        return SizedBox(
          width: width * .8,
          child: Text('Image could not be loaded: $errorPath', softWrap: true),
        );
      }

      return _createFinalWidget(
          baseContext: context,
          imageAndPath: imageResult,
          width: width,
          height: height,
          noImageText: 'No Image',
          onTap: null);
    }

    return const Text('Error creating image widget');
  }

  Future<void> _attemptToPlay(
      BuildContext context, String command, String path) async {
    _logger.fine('Attempting to launch video with $command for $path');
    try {
      await Process.run(command, [path]);
    } catch (e) {
      _logger
          .severe('Exception thrown trying to use $command to play $path: $e');
      SnackBarStatusBuilder.buildSnackbar(
          context, 'Error using $command to play video $path');
    }
  }

  _ImageAndPathResult _uriToImage(String uri, PathMappingService mapper,
      {String imageTypeName = 'image'}) {
    if (uri.startsWith('https://interncache')) {
      return _ImageAndPathResult.none();
    }

    if (uri.startsWith('http')) {
      final networkUrl = uri.toString();
      try {
        return _ImageAndPathResult(Image.network(networkUrl), networkUrl);
      } catch (e) {
        _logger.info(
            'Error trying to create network $imageTypeName: $networkUrl. $e');
      }
      return _ImageAndPathResult.none();
    }

    if (uri.endsWith('mp4')) {
      return _ImageAndPathResult.none();
    }

    final fullPath = mapper.toFullPath(uri.toString());
    final imageFile = File(fullPath);
    if (imageFile.existsSync()) {
      return _ImageAndPathResult(Image.file(imageFile), fullPath);
    }

    return _ImageAndPathResult.none();
  }

  Widget _createFinalWidget(
      {required BuildContext baseContext,
      required _ImageAndPathResult imageAndPath,
      required double width,
      required double height,
      String noImageText = 'No Image',
      String noImageOnTapText = 'No Image',
      required Future<void> Function()? onTap}) {
    final noImage = imageAndPath.image == null;
    final errorText = onTap != null ? noImageOnTapText : noImageText;

    final imageWidget = noImage
        ? Text(errorText,
            style: Theme.of(baseContext)
                .textTheme
                .bodyText2
                ?.copyWith(fontWeight: FontWeight.bold))
        : SizedBox(
            width: width,
            height: height,
            child:
                Image(image: imageAndPath.image!.image, fit: BoxFit.scaleDown),
          );

    if (onTap == null) {
      return imageWidget;
    }

    return InkWell(onTap: onTap, child: imageWidget);
  }

  String _calculatePath(ArchiveServiceProvider archiveService) {
    final url = mediaAttachment.uri.toString();
    String basePath = '';
    if (url.startsWith('http')) {
      final localCacheFile = archiveService.getImageByUrl(url);
      if (localCacheFile.isFailure) {
        return mediaAttachment.uri.toString();
      }

      basePath = localCacheFile.value.localFilename;
    } else {
      basePath = mediaAttachment.uri.path;
    }

    return basePath;
  }
}

class _ImageAndPathResult {
  final Image? image;
  final String path;

  _ImageAndPathResult(this.image, this.path);

  _ImageAndPathResult.none()
      : image = null,
        path = '';
}
