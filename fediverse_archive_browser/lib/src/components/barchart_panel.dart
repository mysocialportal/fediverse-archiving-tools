import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/stat_bin.dart';
import 'package:logging/logging.dart';

class BarChartComponent extends StatelessWidget {
  static final _logger = Logger('$BarChartComponent');
  final List<StatBin> stats;
  final String Function(int index) xLabelMaker;

  const BarChartComponent(
      {Key? key, required this.stats, required this.xLabelMaker})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    _logger.fine("Build BarChartComponent");
    final graphItems = charts.Series<StatBin, String>(
      id: 'Stats',
      domainFn: (bin, _) => xLabelMaker(bin.index),
      measureFn: (bin, _) => bin.count,
      data: stats,
      labelAccessorFn: (bin, _) => bin.count.toString(),
    );

    return AspectRatio(
        aspectRatio: 2,
        child: Card(
            elevation: 4,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
            color: Colors.white,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: charts.BarChart(
                  [graphItems],
                  animate: false,
                  barRendererDecorator: charts.BarLabelDecorator<String>(),
                  domainAxis: const charts.OrdinalAxisSpec(),
                ))));
  }
}
