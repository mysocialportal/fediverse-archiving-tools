import 'package:flutter/material.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';
import 'package:fediverse_archive_browser/src/utils/top_interactors_generator.dart';
import 'package:logging/logging.dart';
import 'package:url_launcher/url_launcher.dart';

class TopInteractorsWidget extends StatefulWidget {
  final List<TimeElement> entries;
  final ConnectionsManager connections;

  const TopInteractorsWidget(this.entries, this.connections, {Key? key})
      : super(key: key);

  @override
  State<TopInteractorsWidget> createState() => _TopInteractionsWidget();
}

class _TopInteractionsWidget extends State<TopInteractorsWidget> {
  static final _logger = Logger('$TopInteractorsWidget');
  int _currentThreshold = 10;
  int _sortIndex = 1;
  final _thresholds = [10, 20, 50, 100];
  final generator = TopInteractorsGenerator();

  @override
  void initState() {
    super.initState();
  }

  void _generateStats() {
    _logger.finer('Filling list');
    generator.clear();
    for (final entry in widget.entries) {
      generator.processEntry(entry.entry, widget.connections);
    }
    _logger.finer('List filled');
    _calcTopList(false);
  }

  Future<void> _calcTopList(bool updateState) async {
    if (updateState) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    _logger.fine('Rebuilding Top Interactors');
    _generateStats();

    final interactors = <InteractorItem>[];

    if (_sortIndex == 1) {
      interactors.addAll(generator.getTopLikes(_currentThreshold));
    } else if (_sortIndex == 2) {
      interactors.addAll(generator.getTopDislikes(_currentThreshold));
    } else {
      interactors.addAll(generator.getTopCommentReshare(_currentThreshold));
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              'Top',
              textAlign: TextAlign.left,
              style: Theme.of(context).textTheme.headline6,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 5.0, right: 5.0),
              child: DropdownButton<int>(
                  value: _currentThreshold,
                  items: _thresholds
                      .map((t) => DropdownMenuItem(value: t, child: Text('$t')))
                      .toList(),
                  onChanged: (newValue) async {
                    _currentThreshold = newValue ?? _thresholds.first;
                    _calcTopList(true);
                  }),
            ),
            Text(
              'Interactors',
              textAlign: TextAlign.right,
              style: Theme.of(context).textTheme.headline6,
            ),
          ],
        ),
        const SizedBox(height: 10.0),
        _buildDataTable(context, interactors),
      ]),
    );
  }

  Widget _buildDataTable(
      BuildContext context, List<InteractorItem> interactors) {
    return DataTable(
      sortColumnIndex: _sortIndex,
      sortAscending: false,
      columns: [
        const DataColumn(label: Text('Name')),
        DataColumn(
            label: const Text('Likes'),
            numeric: true,
            onSort: (column, ascending) => setState(() {
                  _sortIndex = column;
                })),
        DataColumn(
            label: const Text('Dislikes'),
            numeric: true,
            onSort: (column, ascending) => setState(() {
                  _sortIndex = column;
                })),
        DataColumn(
            label: const Text('Reshares'),
            numeric: true,
            onSort: (column, ascending) => setState(() {
                  _sortIndex = column;
                })),
      ],
      rows: List.generate(
          interactors.length,
          (index) => DataRow(
                  color: index.isEven
                      ? MaterialStateProperty.resolveWith(
                          (states) => Theme.of(context).dividerColor)
                      : null,
                  cells: [
                    DataCell(TextButton(
                        onPressed: () async {
                          final url =
                              interactors[index].contact.profileUrl.toString();
                          await canLaunch(url)
                              ? await launch(url)
                              : SnackBarStatusBuilder.buildSnackbar(
                                  context, 'Failed to open $url');
                        },
                        child: Text(interactors[index].contact.name))),
                    DataCell(Text('${interactors[index].likeCount}')),
                    DataCell(Text('${interactors[index].dislikeCount}')),
                    DataCell(
                        Text('${interactors[index].resharedOrCommentedOn}')),
                  ])),
    );
  }
}
