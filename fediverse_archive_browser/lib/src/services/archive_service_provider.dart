import 'package:fediverse_archive_browser/src/diaspora/services/diaspora_archive_service.dart';
import 'package:fediverse_archive_browser/src/diaspora/services/diaspora_path_mapping_service.dart';
import 'package:fediverse_archive_browser/src/friendica/services/friendica_archive_service.dart';
import 'package:fediverse_archive_browser/src/friendica/services/friendica_path_mapping_service.dart';
import 'package:fediverse_archive_browser/src/mastodon/services/mastodon_archive_service.dart';
import 'package:fediverse_archive_browser/src/mastodon/services/mastodon_path_mapping_service.dart';
import 'package:fediverse_archive_browser/src/services/archive_service_interface.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';
import 'package:fediverse_archive_browser/src/services/path_mapper_service_interface.dart';
import 'package:fediverse_archive_browser/src/settings/settings_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:result_monad/result_monad.dart';

import '../models/archive_types_enum.dart';
import '../models/entry_tree_item.dart';
import '../models/local_image_archive_entry.dart';
import '../utils/exec_error.dart';

class ArchiveServiceProvider extends ChangeNotifier implements ArchiveService {
  final SettingsController settings;
  late DiasporaArchiveService _diasporaArchiveService;
  late FriendicaArchiveService _friendicaArchiveService;
  late MastodonArchiveService _mastodonArchiveService;

  @override
  ConnectionsManager get connectionsManager =>
      _archiveService.connectionsManager;

  ArchiveServiceProvider(this.settings) {
    _buildArchiveServices();
  }

  String get ownersName => _archiveService.ownersName;

  void clearCaches() {
    _friendicaArchiveService.clearCaches();
    _diasporaArchiveService.clearCaches();
    _mastodonArchiveService.clearCaches();
    _buildArchiveServices();
  }

  FutureResult<List<EntryTreeItem>, ExecError> getPosts() async {
    return _archiveService.getPosts();
  }

  FutureResult<List<EntryTreeItem>, ExecError> getAllComments() async {
    return _archiveService.getAllComments();
  }

  FutureResult<List<EntryTreeItem>, ExecError> getOrphanedComments() async {
    return _archiveService.getOrphanedComments();
  }

  Result<ImageEntry, ExecError> getImageByUrl(String url) {
    return _archiveService.getImageByUrl(url);
  }

  @override
  PathMappingService get pathMappingService =>
      _archiveService.pathMappingService;

  ArchiveService get _archiveService {
    switch (settings.archiveType) {
      case ArchiveType.diaspora:
        return _diasporaArchiveService;
      case ArchiveType.friendica:
        return _friendicaArchiveService;
      case ArchiveType.mastodon:
        return _mastodonArchiveService;
      default:
        throw Exception('Unknown archive type');
    }
  }

  void _buildArchiveServices() {
    _diasporaArchiveService = DiasporaArchiveService(
        pathMappingService: DiasporaPathMappingService(settings));
    _friendicaArchiveService = FriendicaArchiveService(
        pathMappingService: FriendicaPathMappingService(settings));
    _mastodonArchiveService = MastodonArchiveService(
        pathMappingService: MastodonPathMappingService(settings));
  }
}
