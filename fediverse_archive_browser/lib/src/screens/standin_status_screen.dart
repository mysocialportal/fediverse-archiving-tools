import 'package:flutter/material.dart';

class StandInStatusScreen extends StatelessWidget {
  final String title;
  final String subTitle;

  const StandInStatusScreen({Key? key, required this.title, this.subTitle = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(fontSize: 28),
          softWrap: true,
        ),
        const SizedBox(height: 5),
        if (subTitle.isNotEmpty)
          Text(
            subTitle,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 20),
            softWrap: true,
          ),
      ],
    ));
  }
}
