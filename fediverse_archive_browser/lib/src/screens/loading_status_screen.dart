import 'package:flutter/material.dart';

class LoadingStatusScreen extends StatelessWidget {
  final String title;
  final String subTitle;

  const LoadingStatusScreen({Key? key, required this.title, this.subTitle = ''})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Text(title, style: const TextStyle(fontSize: 18)),
        const SizedBox(height: 20),
        if (subTitle.isNotEmpty) ...[
          Text(subTitle, style: const TextStyle(fontSize: 14)),
          const SizedBox(height: 20)
        ],
        const CircularProgressIndicator()
      ]),
    );
  }
}
