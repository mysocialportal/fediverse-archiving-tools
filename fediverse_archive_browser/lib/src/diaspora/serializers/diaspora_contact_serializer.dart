import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:uuid/uuid.dart';

Connection contactFromDiasporaJson(Map<String, dynamic> json) {
  const network = "Diaspora";
  final accountId = json['account_id'] ?? '';
  final profileUrl = _profileUrlFromAccountId(accountId);
  final name = json['person_name'] ?? '';
  final id = json['person_guid'] ?? '';
  final following = json['following'] ?? false;
  final followed = json['followed'] ?? false;
  var status = ConnectionStatus.none;
  if (following && followed) {
    status = ConnectionStatus.mutual;
  } else if (following) {
    status = ConnectionStatus.youFollowThem;
  } else if (followed) {
    status = ConnectionStatus.theyFollowYou;
  }

  return Connection(
      status: status,
      name: name,
      id: id,
      profileUrl: profileUrl,
      network: network);
}

Connection contactFromDiasporaId(String accountId) => Connection(
    id: 'generated_${const Uuid().v4}',
    status: ConnectionStatus.none,
    profileUrl: _profileUrlFromAccountId(accountId));

Uri _profileUrlFromAccountId(String accountId) {
  if (accountId.isEmpty) {
    return Uri();
  }
  final accountIdPieces = accountId.split('@');
  if (accountIdPieces.length != 2) {
    return Uri();
  }

  final userName = accountIdPieces[0];
  final server = accountIdPieces[1];

  return Uri.parse('https://$server/u/$userName');
}
