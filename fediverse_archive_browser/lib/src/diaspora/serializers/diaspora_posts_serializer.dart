import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:fediverse_archive_browser/src/utils/exec_error.dart';
import 'package:fediverse_archive_browser/src/utils/offsetdatetime_utils.dart';
import 'package:logging/logging.dart';
import 'package:markdown/markdown.dart';
import 'package:result_monad/result_monad.dart';

import '../../services/connections_manager.dart';
import 'diaspora_media_attachment_serializer.dart';

final _logger = Logger('DiasporaPostsSerializer');
const _statusMessageType = 'status_message';
const _reshareType = 'reshare';
const _knownPostTypes = [_statusMessageType, _reshareType];

Result<TimelineEntry, ExecError> timelineItemFromDiasporaPostJson(
    Map<String, dynamic> json, ConnectionsManager connections) {
  if (!json.containsKey('entity_data')) {
    return Result.error(ExecError.message('Timeline item has no entity data'));
  }
  final entityData = json['entity_data'] ?? {};
  final postId = entityData['guid'] ?? '';
  final entityType = json['entity_type'] ?? '';
  if (!_knownPostTypes.contains(entityType)) {
    final error = 'Unknown entity type $entityType for Post ID: $postId';
    _logger.severe(error);
    return Result.error(ExecError.message(error));
  }

  if (entityType == _statusMessageType) {
    return _buildStatusMessageType(entityData, connections);
  } else {
    return _buildReshareMessageType(entityData, connections);
  }
}

Result<TimelineEntry, ExecError> _buildReshareMessageType(
    entityData, ConnectionsManager connections) {
  final createdAtString = entityData['created_at'] ?? '';
  final epochTime =
      OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(createdAtString)
          .getValueOrElse(() => -1);
  final postId = entityData['guid'] ?? '';
  final authorName = entityData['author'] ?? '';
  final String parentGuid = entityData['root_guid'] ?? '';
  final String parentName = entityData['root_author'] ?? '';
  final deletedPost = parentGuid.isEmpty || parentName.isEmpty;
  final reshareLink = deletedPost
      ? ''
      : _buildPostOrReshareUrl(authorName, parentName, parentGuid);
  final text = deletedPost ? 'Original post deleted by author' : '';
  final author =
      connections.getByName(authorName).getValueOrElse(() => Connection());
  final parentAuthor = connections
      .getByName(parentName)
      .getValueOrElse(() => Connection(name: parentName));
  final timelineEntry = TimelineEntry(
      id: postId,
      creationTimestamp: epochTime,
      author: author.name,
      authorId: author.id,
      externalLink: _buildPostOrReshareUrl(authorName, '', postId),
      isReshare: true,
      parentAuthor: parentAuthor.name,
      parentAuthorId: parentAuthor.id,
      body: text,
      links: [Uri.parse(reshareLink)]);
  return Result.ok(timelineEntry);
}

Result<TimelineEntry, ExecError> _buildStatusMessageType(
    entityData, ConnectionsManager connections) {
  final createdAtString = entityData['created_at'] ?? '';
  final epochTime =
      OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(createdAtString)
          .getValueOrElse(() => -1);
  final postId = entityData['guid'] ?? '';
  final postMarkdown = entityData['text'] ?? '';
  final postHtml = markdownToHtml(postMarkdown);
  final authorName = entityData['author'] ?? '';
  final author =
      connections.getByName(authorName).getValueOrElse(() => Connection());
  final photos = (entityData['photos'] as List<dynamic>? ?? [])
      .map((e) => mediaAttachmentfromDiasporaJson(e));

  final timelineEntry = TimelineEntry(
    id: postId,
    creationTimestamp: epochTime,
    body: postHtml,
    author: author.name,
    externalLink: _buildPostOrReshareUrl(authorName, '', postId),
    mediaAttachments: photos.toList(),
    authorId: author.id,
  );
  return Result.ok(timelineEntry);
}

String _buildPostOrReshareUrl(
    String author, String rootAuthor, String rootGuid) {
  final accountId = rootAuthor.isNotEmpty ? rootAuthor : author;
  final accountIdPieces = accountId.split('@');
  if (accountIdPieces.length != 2) {
    return rootGuid;
  }

  final server = accountIdPieces[1];

  return 'https://$server/p/$rootGuid';
}
