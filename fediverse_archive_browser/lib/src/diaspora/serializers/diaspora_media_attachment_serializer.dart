import '../../models/media_attachment.dart';
import '../../utils/offsetdatetime_utils.dart';

MediaAttachment mediaAttachmentfromDiasporaJson(Map<String, dynamic> json) {
  final entityData = json['entity_data'] ?? {};
  final createdAtString = entityData['created_at'] ?? '';
  final creationTimestamp =
      OffsetDateTimeUtils.epochSecTimeFromTimeZoneString(createdAtString)
          .getValueOrElse(() => -1);
  String uriBase = entityData['remote_photo_path'] ?? '';
  final photoName = entityData['remote_photo_name'] ?? '';
  final separator = uriBase.endsWith('/') ? '' : '/';
  final uri = Uri.parse('$uriBase$separator$photoName');
  const explicitType = AttachmentMediaType.image;
  final thumbnailUri = Uri();
  const title = '';
  const description = '';

  return MediaAttachment(
      uri: uri,
      creationTimestamp: creationTimestamp,
      thumbnailUri: thumbnailUri,
      metadata: {},
      title: title,
      explicitType: explicitType,
      description: description);
}
