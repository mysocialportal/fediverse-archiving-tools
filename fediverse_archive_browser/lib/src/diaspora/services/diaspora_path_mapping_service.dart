import 'dart:io';

import 'package:logging/logging.dart';
import 'package:path/path.dart' as p;

import '../../services/path_mapper_service_interface.dart';
import '../../settings/settings_controller.dart';

class DiasporaPathMappingService implements PathMappingService {
  static final _logger = Logger('$DiasporaPathMappingService');
  final SettingsController settings;
  final _photoDirectories = <FileSystemEntity>[];

  DiasporaPathMappingService(this.settings) {
    refresh();
  }

  @override
  String get rootFolder => settings.rootFolder;

  @override
  List<FileSystemEntity> get archiveDirectories =>
      List.unmodifiable(_photoDirectories);

  @override
  void refresh() {
    _logger.fine('Refreshing path mapping service directory data.');
    if (!Directory(settings.rootFolder).existsSync()) {
      _logger.severe(
          "Base directory does not exist! can't do mapping of ${settings.rootFolder}");
      return;
    }
    _photoDirectories.clear();

    _photoDirectories.addAll(Directory(settings.rootFolder)
        .listSync(recursive: false)
        .where((element) =>
            element.statSync().type == FileSystemEntityType.directory));
  }

  @override
  String toFullPath(String relPath) {
    if (File(relPath).existsSync()) {
      return relPath;
    }

    for (final file in _photoDirectories) {
      final fullPath = p.join(file.path, relPath);
      if (File(fullPath).existsSync()) {
        return fullPath;
      }
    }

    _logger.fine(
        'Did not find a file with this relPath anywhere therefore returning the relPath');
    return relPath;
  }
}
