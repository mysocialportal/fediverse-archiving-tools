import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fediverse_archive_browser/src/utils/snackbar_status_builder.dart';

Future<void> copyToClipboard(
    {required BuildContext context,
    required String text,
    required String snackbarMessage}) async {
  await Clipboard.setData(ClipboardData(text: text));
  SnackBarStatusBuilder.buildSnackbar(context, snackbarMessage);
}
