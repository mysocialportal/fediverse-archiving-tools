import 'package:fediverse_archive_browser/src/models/stat_bin.dart';
import 'package:fediverse_archive_browser/src/models/time_element.dart';

class TimeStatGenerator {
  final List<TimeElement> _elements;

  TimeStatGenerator(Iterable<TimeElement> items) : _elements = items.toList() {
    _elements.sort((e1, e2) => e1.timestamp.compareTo(e2.timestamp));
  }

  List<TimeElement> get sortedElements => List.unmodifiable(_elements);

  List<StatBin> calculateDailyStats() {
    final result = <StatBin>[];
    final interimBins = <DateTime, int>{};
    for (final element in _elements) {
      final day = element.timestamp.toDayOnly();
      final currentSum = interimBins[day] ?? 0;
      interimBins[day] = currentSum + 1;
    }

    for (final bin in interimBins.entries) {
      result.add(StatBin(index: 0, binEpoch: bin.key, initialCount: bin.value));
    }

    result.sort((a, b) => a.binEpoch.compareTo(b.binEpoch));

    return result;
  }

  List<StatBin> calculateByDayOfWeekStats() => _calculateStats(
      binCount: 7,
      elementToTimeIndex: (e) => e.timestamp.weekday,
      timeIndexToArrayIndex: (ti) => ti - 1,
      arrayIndexToTimeIndex: (ai) => ai + 1);

  List<StatBin> calculateByMonthStats() => _calculateStats(
      binCount: 12,
      elementToTimeIndex: (e) => e.timestamp.month,
      timeIndexToArrayIndex: (ti) => ti - 1,
      arrayIndexToTimeIndex: (ai) => ai + 1);

  List<StatBin> calculateStatsByYear() {
    if (_elements.isEmpty) {
      return [];
    }
    final earliestYear = _elements.first.timestamp.year;
    final latestYear = _elements.last.timestamp.year;
    final binCount = latestYear - earliestYear + 1;
    return _calculateStats(
        binCount: binCount,
        elementToTimeIndex: (e) => e.timestamp.year,
        timeIndexToArrayIndex: (ti) => ti - earliestYear,
        arrayIndexToTimeIndex: (ai) => ai + earliestYear);
  }

  List<StatBin> _calculateStats(
      {required int binCount,
      required int Function(TimeElement) elementToTimeIndex,
      required int Function(int) timeIndexToArrayIndex,
      required int Function(int) arrayIndexToTimeIndex}) {
    final bins = List.generate(binCount, (index) {
      final timeIndex = arrayIndexToTimeIndex(index);
      return StatBin(index: timeIndex);
    });

    for (final e in _elements) {
      final arrayIndex = timeIndexToArrayIndex(elementToTimeIndex(e));
      bins[arrayIndex].increment();
    }

    return bins;
  }
}

extension DateTimeToDateOnly on DateTime {
  DateTime toDayOnly() => DateTime(year, month, day);
}
