import 'package:fediverse_archive_browser/src/models/connection.dart';
import 'package:fediverse_archive_browser/src/models/timeline_entry.dart';
import 'package:fediverse_archive_browser/src/services/connections_manager.dart';

class TopInteractorsGenerator {
  final _interactors = <String, InteractorItem>{};
  final _processedEntryIds = <String>{};

  void clear() {
    _interactors.clear();
    _processedEntryIds.clear();
  }

  void processEntry(TimelineEntry item, ConnectionsManager contacts) {
    if (_processedEntryIds.contains(item.id)) {
      return;
    }

    _processedEntryIds.add(item.id);

    if (item.parentAuthorId.isNotEmpty) {
      final interactorItem =
          _getInteractorItemById(item.parentAuthorId, contacts);
      _interactors[item.parentAuthorId] =
          interactorItem.incrementResharedOrCommentedOn();
    }

    for (final like in item.likes) {
      final interactorItem =
          _interactors[like.id] ?? InteractorItem(contact: like);
      _interactors[like.id] = interactorItem.incrementLike();
    }

    for (final dislike in item.dislikes) {
      final interactorItem =
          _interactors[dislike.id] ?? InteractorItem(contact: dislike);
      _interactors[dislike.id] = interactorItem.incrementDislike();
    }
  }

  List<InteractorItem> getTopCommentReshare(int threshold) {
    final forResult = List.of(_interactors.values);
    forResult.sort((i1, i2) =>
        i2.resharedOrCommentedOn.compareTo(i1.resharedOrCommentedOn));
    return forResult.take(threshold).toList();
  }

  List<InteractorItem> getTopLikes(int threshold) {
    final forResult = List.of(_interactors.values);
    forResult.sort((i1, i2) => i2.likeCount.compareTo(i1.likeCount));
    return forResult.take(threshold).toList();
  }

  List<InteractorItem> getTopDislikes(int threshold) {
    final forResult = List.of(_interactors.values);
    forResult.sort((i1, i2) => i2.dislikeCount.compareTo(i1.dislikeCount));
    return forResult.take(threshold).toList();
  }

  InteractorItem _getInteractorItemById(
      String id, ConnectionsManager contacts) {
    if (_interactors.containsKey(id)) {
      return _interactors[id]!;
    }

    final contact = contacts.getById(id).fold(
        onSuccess: (contact) => contact,
        onError: (error) => Connection(
            status: ConnectionStatus.none,
            name: '',
            id: id,
            profileUrl: Uri(),
            network: 'network'));
    return InteractorItem(contact: contact);
  }
}

class InteractorItem {
  final Connection contact;
  final int resharedOrCommentedOn;
  final int likeCount;
  final int dislikeCount;

  InteractorItem(
      {required this.contact,
      this.resharedOrCommentedOn = 0,
      this.likeCount = 0,
      this.dislikeCount = 0});

  @override
  String toString() {
    return 'InteractorItem{contact: $contact, resharedOrCommentedOn: $resharedOrCommentedOn, likeCount: $likeCount, dislikeCount: $dislikeCount}';
  }

  InteractorItem copy(
      {Connection? contact,
      int? resharedOrCommentedOn,
      int? likeCount,
      int? dislikeCount}) {
    return InteractorItem(
        contact: contact ?? this.contact,
        resharedOrCommentedOn:
            resharedOrCommentedOn ?? this.resharedOrCommentedOn,
        likeCount: likeCount ?? this.likeCount,
        dislikeCount: dislikeCount ?? this.dislikeCount);
  }

  InteractorItem incrementResharedOrCommentedOn() =>
      copy(resharedOrCommentedOn: this.resharedOrCommentedOn + 1);

  InteractorItem incrementLike() => copy(likeCount: this.likeCount + 1);

  InteractorItem incrementDislike() =>
      copy(dislikeCount: this.dislikeCount + 1);
}
