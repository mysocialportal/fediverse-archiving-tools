# Friendica Archiver

A simple command line tool for archiving your Friendica profile via the Friendica API.

## Installation

There are pre-compiled binaries for the tools for Linux, Mac, and Windows below. You can download
those and use them. Simply download for your respective platform, unzip, and then run.
* [Windows x64](https://mysocialportal-fediverse-archiving.sfo3.digitaloceanspaces.com/friendica_archiver/1.2.0/friendica_archiver_v1.2.0_win_x64.zip)
* [macOS x64](https://mysocialportal-fediverse-archiving.sfo3.digitaloceanspaces.com/friendica_archiver/1.2.0/friendica_archiver_v1.2.0_macos_x64.zip)
* [Linux x64](https://mysocialportal-fediverse-archiving.sfo3.digitaloceanspaces.com/friendica_archiver/1.2.0/friendica_archiver_v1.2.0_linux_x64.zip)

**Note: The binaries have a .exe extension on all platforms even though it is only significant on Windows.**

Or you can run it directly from this directory using standard Dart commands:

```bash
git clone git@gitlab.com:mysocialportal/fediverse-archiving-tools.git
cd fediverse-archiving-tools
dart pub get
dart bin/friendica_archiver.dart
```

## Usage

The tool only supports username and password credentials workflow. The arguments to run are:

Online help:
```bash
-a, --archive-folder (mandatory)    Specifies the local folder all data files pulled from the server will be stored
-u, --username (mandatory)          Username on your Friendica instance
-s, --server-name (mandatory)       The server name for your instance. (e.g. if the URL in your browser is "https://friendica.com/" then this would be "friendica.com
-r, --resume-page                   The page to restart the downloading process. Will try to read in existing posts and image archive data and start download from there. If set to 0 it resets from scratch.
                                    (defaults to "0")
-d, --delay                         Delay in milliseconds between requests to try not to stress the server (thousands of API calls can be made)
                                    (defaults to "5000")
-m, --max-post-requests             The maximum number of times to query for posts
                                    (defaults to "1000000000")
-p, --items-per-page                The requested number of items per page
                                    [1, 5, 10, 20 (default), 50, 100]
-i, --[no-]download-images          Whether to download images from posts when those images are stored on the server (not links to other sites) (defaults to true)
                                    (defaults to on)
```

Along with the mandatory arguments you will be required to provide a password. 
There will a prompt for the password which one can enter in. Alternatively one can store their password
in a file and pass it in via stdin redirect.

The base archive folder needs to be created first and should be an empty folder if starting an archival 
from scratch.

## Examples

### Basic Usage
Minimum arguments to start processing the archive from scratch:

```bash
./friendica_archiver --username user1 --server-name myfriendicaserver.com --archive-folder ~/Downloads/FriendicaArchive
```

In this case the Password prompt will come up nad you type in your profile. 
There is a current bug with Dart that prevents me from disabling echo so your password will
be displayed on the screen.

Minimum arguments to start processing the archive with password in file:

```bash
./friendica_archiver --username user1 --server-name myfriendicaserver.com --archive-folder ~/Downloads/FriendicaArchive < ~/friendica_password.txt
```

In this case if your password is `123456789` (and please change your password if that is your actual password),
then the entirety of the text file would be:

```
123456789
```

### Restarting Archival

As the archival unfolds it lists which pages it outputs status about which page it loaded last.
It is therefore possible to restart archiving at a certain page:

```bash
./friendica_archiver --username user1 --server-name myfriendicaserver.com --archive-folder ~/Downloads/FriendicaArchive --resume-page 2 --max-post-requests 3 --items-per-page 5  < ~/friendica_password.txt
```
